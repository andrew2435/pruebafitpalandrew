import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AgregarClientePage } from '../pages/agregar-cliente/agregar-cliente';
import { ListadoClientePage } from '../pages/listado-cliente/listado-cliente';
import { ClientePage } from '../pages/cliente/cliente';

import { MyApp } from './app.component';
import { QueriesProvider } from '../providers/queries/queries';
import { HttpModule } from "@angular/http";

@NgModule({
  declarations: [
    MyApp,
    AgregarClientePage,
    ListadoClientePage,
    ClientePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AgregarClientePage,
    ListadoClientePage,
    ClientePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    QueriesProvider
  ]
})
export class AppModule {}
