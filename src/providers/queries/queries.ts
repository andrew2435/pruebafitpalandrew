import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class QueriesProvider {
  apiUrl = "https://zeus.checklemon.com/api/test/users"
  apiDepartamentoUrl = "https://zeus.checklemon.com/api/test/departments";
  apiCiudadesDepartamentoUrl = "https://zeus.checklemon.com/api/test/cities?department=";

  constructor(public http: Http) {
  }

  cargarClientes() {
    return Observable.create(observer => {
      let headers = new Headers();
      this.http.get(this.apiUrl, { headers: headers }).map(res => res.json()).subscribe(data => {
        observer.next(data);
      }, (err) => {
        observer.error(err);
      });
    });
  }

  cargarDepartamentos() {
    return Observable.create(observer => {
      let headers = new Headers();
      this.http.get(this.apiDepartamentoUrl, { headers: headers }).map(res => res.json()).subscribe(data => {
        observer.next(data);
      }, (err) => {
        observer.error(err);
      });
    });
  }

  cargarCiudadesXDepartamento(departamento) {
    return Observable.create(observer => {
      let headers = new Headers();
      this.http.get(this.apiCiudadesDepartamentoUrl + departamento, { headers: headers }).map(res => res.json()).subscribe(data => {
        observer.next(data);
      }, (err) => {
        observer.error(err);
      });
    });
  }

  crearNuevoUsuario(data: any) {
    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      let data_binary = {
        name: data.name,
        last_name: data.last_name,
        email: data.email,
        doc_type: data.doc_type,
        doc_number: data.doc_number,
        city_id: data.city_id,
      };
      this.http.post(this.apiUrl, data_binary, { headers: headers }).subscribe(data => {
        observer.next(data);
      }, (err) => {
        observer.error(err);
      });
    });
  }

}
