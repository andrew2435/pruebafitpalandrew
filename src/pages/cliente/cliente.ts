import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-cliente',
  templateUrl: 'cliente.html',
})
export class ClientePage {
  infoCliente:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.infoCliente = this.navParams.get('cliente');
    console.log(this.infoCliente)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientePage');
  }

}
