import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { QueriesProvider } from '../../providers/queries/queries';
import { ClientePage } from '../../pages/cliente/cliente';

@Component({
  selector: 'page-listado-cliente',
  templateUrl: 'listado-cliente.html',
})
export class ListadoClientePage {
  clientes: any;
  loading: any;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public queries: QueriesProvider
  ) {
    this.inicializarDatos();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListadoClientePage');
  }

  inicializarDatos() {
    this.showLoader();

    this.queries.cargarClientes().subscribe(data => {
      this.clientes = data ? data : [];
      console.log(this.clientes)
      this.loading.dismiss();
    }, error => {
      this.loading.dismiss();
      console.log(error)
    });
  }

  irDetalleCliente(cliente) {
    this.navCtrl.push(
      ClientePage,
      { cliente: cliente },
      { animate: true }
    );
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Cargando...'
    });
    this.loading.present();
  }

}
