import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QueriesProvider } from '../../providers/queries/queries';
import { ListadoClientePage } from '../../pages/listado-cliente/listado-cliente';

@Component({
  selector: 'page-agregar-cliente',
  templateUrl: 'agregar-cliente.html',
})
export class AgregarClientePage {
  public myForm: FormGroup;
  loading: any;
  mensaje: string;
  listaCiudades: any;
  listaDepartamentos: any;
  existeDepartamento: boolean = true;
  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public navParams: NavParams,
    public queries: QueriesProvider
  ) {
    this.myForm = this.createMyForm();
    this.cargarDepartamentos();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgregarClientePage');
  }

  createMyForm() {
    return this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(30)])],
      last_name: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(30)])],
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$')])],
      doc_type: ['', Validators.compose([Validators.required])],
      doc_number: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])],
      city_id: ['', Validators.compose([Validators.required])],
    });
  }

  cargarDepartamentos() {
    this.queries.cargarDepartamentos().subscribe(data => {
      this.listaDepartamentos = data ? data : [];
    }, error => {
      console.log(error)
    });
  }

  cargarCiudadesXDepartamento(idDepartamento) {
    this.queries.cargarCiudadesXDepartamento(idDepartamento).subscribe(data => {
      this.listaCiudades = data ? data : [];
      this.existeDepartamento = this.listaCiudades.length > 0 ? false : true;
    }, error => {
      console.log(error)
    });
  }

  crearUsuario() {
    this.showLoader();
    if (!this.myForm.valid) {
      this.loading.dismiss();
      this.presentToast('Por favor llenar todos los datos del formulario');
    } else {
      this.queries.crearNuevoUsuario(this.myForm.value).subscribe(data => {
        this.loading.dismiss();
        console.log("El usuario se guadó correctamente!", data)
        this.presentToast("El usuario se guadó correctamente!");
        this.navCtrl.push(ListadoClientePage);
      }, error => {
        this.loading.dismiss();
        let errorCode = error;
        if (errorCode) {
          switch (errorCode) {
            case 500:
              this.mensaje = 'Error al crear el cliente por favor verifique los campos.';
              break;
            default:
              this.mensaje = 'Error al crear el cliente. Comuniquese con el área de soporte. ';
          }
          this.presentToast(this.mensaje);
        }
      });
    }
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Cargando...'
    });
    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  irListaCliente(){
    this.navCtrl.push(ListadoClientePage);
  }

}
